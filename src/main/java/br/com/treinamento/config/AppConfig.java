package br.com.treinamento.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import br.com.treinamento.dojo.controller.MarvelAPIController;

@Configuration
@EnableAutoConfiguration
@ComponentScan("br.com.treinamento.dojo")
public class AppConfig {
	public static void main(String[] args) {
		SpringApplication.run(AppConfig.class, args);
		
		new MarvelAPIController().loadLists();
	}
}
