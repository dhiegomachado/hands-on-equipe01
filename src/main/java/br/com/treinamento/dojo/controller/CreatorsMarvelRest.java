package br.com.treinamento.dojo.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.domain.Creator;

@RestController(value = "/marvel/creators/")
public class CreatorsMarvelRest {

	Map<Integer, Creator> creatorsList = new HashMap<>();
	
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<List<Creator>> get(
			@RequestParam(value = "idCreator", required = false) Integer idCreator) {
		
		
		if (idCreator == null) {
			return new ResponseEntity<List<Creator>>(new ArrayList<Creator> (creatorsList.values()), HttpStatus.OK);	
		} else {
			
			return new ResponseEntity<List<Creator>>(Arrays.asList(creatorsList.get(idCreator)), HttpStatus.OK);
		}		
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON,
			produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<Creator> edit(
			@RequestBody(required = true) Creator creator) {
		Creator oCreator = creatorsList.get(creator.getId());
		BeanUtils.copyProperties(creator, oCreator);
		return new ResponseEntity<Creator>(creator, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON,
			produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<Creator> save(
			@RequestBody(required = true) Creator creator) {
		creator.setId((int) (Math.random() * 1000));
		creatorsList.put(creator.getId(), creator);	
		return new ResponseEntity<Creator>(creator, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<String> delete(
			@RequestParam(value = "idCreator", required = true) Integer idCreator) {
		creatorsList.remove(idCreator);
		return new ResponseEntity<String>("Registro removido!", HttpStatus.OK);
	}

}
