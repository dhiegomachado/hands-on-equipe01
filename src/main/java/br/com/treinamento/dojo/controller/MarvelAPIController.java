package br.com.treinamento.dojo.controller;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Controller;

import br.com.treinamento.domain.Comic;
import br.com.treinamento.domain.Creator;

@Controller
public class MarvelAPIController {

	private List<Comic> comics;
	private List<Creator> creators;

	private static final String PUBLIC_KEY = "65acec9e3f037e6b5deafe43bc75d599";
	private static final String PRIVATE_KEY = "539c572c22fe3699e3035396b6cf53132ba962e6";
	private static final String MARVEL_ENDPOINT_CREATORS = "https://gateway.marvel.com/v1/public/creators";
	private static final String MARVEL_ENDPOINT_COMICS = "https://gateway.marvel.com/v1/public/comics";
	
	
	public void loadLists(){
		doGetCreators();
		doGetComics();
	}
	
	private void doGetCreators(){
		
		try {

			MessageDigest md = MessageDigest.getInstance("MD5");

			long time = Calendar.getInstance().getTimeInMillis();
			byte[] message = (time + PRIVATE_KEY +PUBLIC_KEY).getBytes("UTF-8");
		    String hash = new String(md.digest(message));

		    URL obj = new URL(MARVEL_ENDPOINT_CREATORS+"?ts="+time+"&apikey="+PUBLIC_KEY+"&hash="+hash);
		    InputStream is = obj.openStream();
		    
		    BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
		    StringBuilder sb = new StringBuilder();
		    String line;
		    while ((line = rd.readLine()) != null) {
		    	sb.append(line + "\n");
		    }
		    rd.close();
		    // GET JSON
		    System.out.println(sb.toString());
		 } catch ( Exception cnse) {
		     cnse.printStackTrace();
		 }
		
	}

	private void doGetComics(){
		
		try {
			
			MessageDigest md = MessageDigest.getInstance("MD5");
			
			long time = Calendar.getInstance().getTimeInMillis();
			byte[] message = (time + PRIVATE_KEY +PUBLIC_KEY).getBytes("UTF-8");
			String hash = new String(md.digest(message));
			
			URL obj = new URL(MARVEL_ENDPOINT_COMICS+"?ts="+time+"&apikey="+PUBLIC_KEY+"&hash="+hash);
			InputStream is = obj.openStream();
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line + "\n");
			}
			rd.close();
			// GET JSON
			System.out.println(sb.toString());
		} catch ( Exception cnse) {
			cnse.printStackTrace();
		}
		
	}
	
	public List<Comic> getComics() {
		return comics;
	}
	public void setComics(List<Comic> comics) {
		this.comics = comics;
	}
	public List<Creator> getCreators() {
		return creators;
	}
	public void setCreators(List<Creator> creators) {
		this.creators = creators;
	}
	
}
